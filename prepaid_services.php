<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 30.07.17
 * Time: 10:47
 */

require 'connect_to_db.php';

if($Personal_Account_ID != null) {
    require 'libs/Smarty.class.php';
    $smarty = new Smarty;

    $params = array(array(NULL, SQLSRV_PARAM_IN),
        array($Personal_Account_ID, SQLSRV_PARAM_IN));
    $tsql = "{ CALL dbo.CUSTOMER_PURCHASED_SERVICE_GetList( ?, ? ) }";
    $stmt = sqlsrv_query( $conn, $tsql, $params);
    if( $stmt === false )
    {
        echo "Error in executing query.</br>";
        die( print_r( sqlsrv_errors(), true));
    }
    $LoadList = array();
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            $LoadList[] = $row;
    }


    $smarty->debugging = false;
    $smarty->caching = false;
    $smarty->cache_lifetime = 300;

    $smarty->assign("Title", 'Предоплаченые сервисы');
    $smarty->assign("LoadList", $LoadList);
    $smarty->display('prepaid_services.tpl');
} else {
    header('Location:/index.php');
    exit;
}
?>