<?php
/* Smarty version 3.1.28, created on 2017-08-06 10:48:38
  from "/var/www/templates/prepaid_services.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5986c9d64f12f3_74208512',
  'file_dependency' => 
  array (
    '3a9b254b5e86ae498d134ebe3710635d987f79dd' => 
    array (
      0 => '/var/www/templates/prepaid_services.tpl',
      1 => 1502005714,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5986c9d64f12f3_74208512 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<div class="container">
    <h2><?php echo $_smarty_tpl->tpl_vars['Title']->value;?>
</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Услуга</th>
            <th>Количество</th>
        </tr>
        </thead>
        <tbody>

        <?php
$_from = $_smarty_tpl->tpl_vars['LoadList']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['value']->value["Service_Name"];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['value']->value["Service_Count"];?>
</td>

        </tr>
        <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
?>
        </tbody>
    </table>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-0 col-xs-offset-0 ">

            <p></p>

            <p><a href="cabinet.php" class="btn btn-success center-block">Назад в кабинет</a></p>

            <p><a href="index.php" class="btn btn-success center-block">Назад к списку взлетов</a></p>
        </div>
    </div>

</div>


<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
