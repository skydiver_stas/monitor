<?php
/* Smarty version 3.1.28, created on 2017-08-06 10:15:03
  from "/var/www/templates/index.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5986c1f72c8a01_99359961',
  'file_dependency' => 
  array (
    'ff424573a2acea654f0007665e4557fa8dbdd373' => 
    array (
      0 => '/var/www/templates/index.tpl',
      1 => 1502003694,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:register.tpl' => 1,
    'file:exercise.tpl' => 1,
    'file:error.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5986c1f72c8a01_99359961 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- вход или выход -->
<div class="monitor-page-title">
	<?php if ($_smarty_tpl->tpl_vars['Personal_Account_ID']->value != 0) {
echo $_smarty_tpl->tpl_vars['Personal_Account_Name']->value;?>
 <span class="logout-link-holder">(<a class="logout-link" href="logout.php">Выйти</a>)</span><a href="cabinet.php">&nbsp;&nbsp;&nbsp;Войти в кабинет</a>
	<?php } else {
echo $_smarty_tpl->tpl_vars['Title']->value;?>
 <a class="signup-link" href="#login-form">Войти</a><?php }?>
</div>

<!-- информация о текущем взлете -->
<div class="monitor-page-infobar clearfix">
	<div class='monitor-page-infobar-item'>Взлет № <?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_Number'];?>
&nbsp;</div>
	<div class='monitor-page-infobar-item'>Свободно <?php if ($_smarty_tpl->tpl_vars['CurrentLoad']->value['LoadQty'] > 0) {
echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['LoadQty'];
} else { ?>нет<?php }?>&nbsp;<?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['Seats'];?>
</div>
	<div class='monitor-page-infobar-item'>Осталось <?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['MINUTES'];?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['Minutes']->value;?>
</div>
</div>


<!-- список взлета -->
<?php $_smarty_tpl->tpl_vars['Maxload'] = new Smarty_Variable(16, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Maxload', 0);?>
<form class="own-form" name="RegForm" method="POST">
	<input name="Load_ID" value="<?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_ID'];?>
" type="hidden">
<table class="checkintable"><tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['LoadList']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_customer_0_saved_item = isset($_smarty_tpl->tpl_vars['customer']) ? $_smarty_tpl->tpl_vars['customer'] : false;
$_smarty_tpl->tpl_vars['customer'] = new Smarty_Variable();
$__foreach_customer_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_customer_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['customer']->value) {
$__foreach_customer_0_saved_local_item = $_smarty_tpl->tpl_vars['customer'];
?>
	<tr class="checkintable-row <?php if (($_smarty_tpl->tpl_vars['customer']->value['Rank'] % 2)) {?>zebra-dark<?php }?>">
		<td class="checkintable-spacecell"></td>
		<td class="checkintable-cell checkintable-rownum"><?php echo $_smarty_tpl->tpl_vars['customer']->value['RowNum'];?>
</td>
		<td class="checkintable-cell checkintable-name"><?php echo $_smarty_tpl->tpl_vars['customer']->value['Customer_Name'];?>
&nbsp;
		<?php if ($_smarty_tpl->tpl_vars['Personal_Account_ID']->value != 0 && ($_smarty_tpl->tpl_vars['Customer_Type_ID']->value == 2 || $_smarty_tpl->tpl_vars['Customer_Type_ID']->value == 6) && $_smarty_tpl->tpl_vars['Personal_Account_ID']->value == $_smarty_tpl->tpl_vars['customer']->value['Owner_ID']) {?>
			<a href="#">Заполнить бронь</a>
			<input name="Load_List_ID" value="<?php echo $_smarty_tpl->tpl_vars['customer']->value['Load_List_ID'];?>
" type="hidden"><?php }?></td>
		<td class="checkintable-cell checkintable-exercise"><?php echo $_smarty_tpl->tpl_vars['customer']->value['Exercise_Name'];?>
</td>
		<td class="checkintable-spacecell"></td>
	</tr>
<?php $_smarty_tpl->tpl_vars['Maxload'] = new Smarty_Variable($_smarty_tpl->tpl_vars['Maxload']->value-1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Maxload', 0);
$_smarty_tpl->tpl_vars['customer'] = $__foreach_customer_0_saved_local_item;
}
} else {
?>
	<tr class="checkintable-row"><td class="checkintable-cell" colspan="3" align="center">Вы можете быть первым!</td></tr>
	<?php $_smarty_tpl->tpl_vars['Maxload'] = new Smarty_Variable(15, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'Maxload', 0);
}
if ($__foreach_customer_0_saved_item) {
$_smarty_tpl->tpl_vars['customer'] = $__foreach_customer_0_saved_item;
}
$_smarty_tpl->tpl_vars['customer'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['customer']->step = 1;$_smarty_tpl->tpl_vars['customer']->total = (int) ceil(($_smarty_tpl->tpl_vars['customer']->step > 0 ? $_smarty_tpl->tpl_vars['Maxload']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['Maxload']->value)+1)/abs($_smarty_tpl->tpl_vars['customer']->step));
if ($_smarty_tpl->tpl_vars['customer']->total > 0) {
for ($_smarty_tpl->tpl_vars['customer']->value = 1, $_smarty_tpl->tpl_vars['customer']->iteration = 1;$_smarty_tpl->tpl_vars['customer']->iteration <= $_smarty_tpl->tpl_vars['customer']->total;$_smarty_tpl->tpl_vars['customer']->value += $_smarty_tpl->tpl_vars['customer']->step, $_smarty_tpl->tpl_vars['customer']->iteration++) {
$_smarty_tpl->tpl_vars['customer']->first = $_smarty_tpl->tpl_vars['customer']->iteration == 1;$_smarty_tpl->tpl_vars['customer']->last = $_smarty_tpl->tpl_vars['customer']->iteration == $_smarty_tpl->tpl_vars['customer']->total;?>
	<tr class="checkintable-row">
		<td class="checkintable-spacecell"></td>
		<td class="checkintable-cell checkintable-rownum">&nbsp;</td>
		<td class="checkintable-cell" colspan="1">&nbsp;</td>
		<td class="checkintable-cell checkintable-exercise">&nbsp;</td>
		<td class="checkintable-spacecell"></td>
	</tr>
<?php }
}
?>

</tbody></table>
</form>


<!-- навигация между взлетами -->
<div class="checkintable-nav-holder">
	<form class="checkintable-nav clearfix" action="" method="post">
		<?php
$_from = $_smarty_tpl->tpl_vars['Loads']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$__foreach_value_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_value_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
			<?php if ($_smarty_tpl->tpl_vars['value']->value['Load_ID'] == $_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_ID']) {?>
				<div class="button checkintable-nav-bullet active" type="submit" name="Load_ID" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['Load_ID'];?>
"><?php echo preg_replace('!\s+!', '',$_smarty_tpl->tpl_vars['value']->value['Load_Number']);?>
-й взлет<span class="checkintable-nav-bullet-note"><?php if ($_smarty_tpl->tpl_vars['value']->value['LoadQty'] > 0) {
echo $_smarty_tpl->tpl_vars['value']->value['LoadQty'];
} else { ?>нет<?php }?> <?php echo $_smarty_tpl->tpl_vars['value']->value['Seats'];?>
</span></div>
			<?php } else { ?>
				<button class="checkintable-nav-bullet" type="submit" name="Load_ID" value="<?php echo $_smarty_tpl->tpl_vars['value']->value['Load_ID'];?>
"><?php echo preg_replace('!\s+!', '',$_smarty_tpl->tpl_vars['value']->value['Load_Number']);?>
-й взлет<span class="checkintable-nav-bullet-note"><?php if ($_smarty_tpl->tpl_vars['value']->value['LoadQty'] > 0) {
echo $_smarty_tpl->tpl_vars['value']->value['LoadQty'];
} else { ?>нет<?php }?> <?php echo $_smarty_tpl->tpl_vars['value']->value['Seats'];?>
</span></button>
			<?php }?>
		<?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
?>
	</form>
</div>



<!-- кнопка записи/выписки во взлет -->
<?php if ($_smarty_tpl->tpl_vars['Personal_Account_ID']->value != 0 && ($_smarty_tpl->tpl_vars['Customer_Type_ID']->value == 2 || $_smarty_tpl->tpl_vars['Customer_Type_ID']->value == 5 || $_smarty_tpl->tpl_vars['Customer_Type_ID']->value == 6)) {?>
	<form class="reg-form" name="RegForm" method="POST">
		<div class="checkin-controls">
		<div class="checkin-control-holder">
			<?php if (!$_smarty_tpl->tpl_vars['InLoad']->value) {?>
				<?php if ($_smarty_tpl->tpl_vars['CurrentLoad']->value['LoadQty'] == 0 || $_smarty_tpl->tpl_vars['CurrentLoad']->value['Rezerved']) {?><div class="button checkin-button disabled">Записаться в <?php echo preg_replace('!\s+!', '',$_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_Number']);?>
-й взлет</div>
				<?php } else {
if ($_smarty_tpl->tpl_vars['CurrentLoad']->value['MINUTES'] >= 15) {?><a class="button checkin-button" href="#exercise-dialog">Записаться в <?php echo preg_replace('!\s+!', '',$_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_Number']);?>
-й взлет</a>
					<?php } else { ?><div class="button checkin-button disabled">До <?php echo preg_replace('!\s+!', '',$_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_Number']);?>
-го взлета осталось <?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['MINUTES'];?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['Minutes']->value;?>
</div>
					<?php }?>
				<?php }?>
			<?php } else { ?>
				<?php if ($_smarty_tpl->tpl_vars['CurrentLoad']->value['MINUTES'] >= 15) {?><button name="reg" value="unreg">Выписаться</button><?php }?>
			<?php }?>
		</div>
		</div>
		<input name="Load_ID" value="<?php echo $_smarty_tpl->tpl_vars['CurrentLoad']->value['Load_ID'];?>
" type="hidden">
	</form>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['Personal_Account_ID']->value == 0) {?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:register.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } else { ?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:exercise.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['msg']->value)) {?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:error.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 src='js/main.js'><?php echo '</script'; ?>
>
<?php }
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
