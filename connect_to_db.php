<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 29.07.17
 * Time: 13:37
 */

/* Specify the server and connection string attributes. */
$serverName = "10.3.29.26";

/* Get UID and PWD from application-specific files.  */
$connectionInfo = array( "UID"=>"sa", "PWD"=>"dsn1","Database"=>"manifesto");

/* Connect using SQL Server Authentication. */
$conn = sqlsrv_connect($serverName, $connectionInfo);
if( $conn === false )
{
    echo "Unable to connect.</br>";
    die( print_r( sqlsrv_errors(), true));
}

/* Query SQL Server for the login of the user accessing the database. */
//select all variables from POST array into the same PHP variables
if(isset($_POST)){
    foreach ($_POST as $key=>$val){
        $$key=$val;
    }
}


$Personal_Account_ID = 0;
$Personal_Account_Name = '';
$Customer_Type_ID = 0;
if (!isset($_COOKIE["Personal_Account_ID"]) ||	isset($login)) {
    $tsql = "{ CALL dbo.TERM_AUTHCARD (?,?,?,?,?) }";
    $params = [
        [$login[username], SQLSRV_PARAM_IN],
        [$login[password], SQLSRV_PARAM_IN],
        [&$Personal_Account_ID, SQLSRV_PARAM_OUT, SQLSRV_PHPTYPE_INT],
        [&$Personal_Account_Name, SQLSRV_PARAM_OUT, SQLSRV_PHPTYPE_STRING('UTF-8'),SQLSRV_SQLTYPE_VARCHAR(254)],
        [&$Customer_Type_ID, SQLSRV_PARAM_OUT, SQLSRV_PHPTYPE_INT]
    ];

    $stmt = sqlsrv_query( $conn, $tsql, $params);
    if( $stmt === false )
    {
        echo "Error in executing query.</br>";
        die( print_r( sqlsrv_errors(), true));
    }
    setcookie("Personal_Account_ID", $Personal_Account_ID, time()+3600*48);
    setcookie("Personal_Account_Name", $Personal_Account_Name, time()+3600*48);
    setcookie("Customer_Type_ID",$Customer_Type_ID, time()+3600*48);
}
else {
    $Personal_Account_ID = intval($_COOKIE["Personal_Account_ID"]);
    $Personal_Account_Name = $_COOKIE["Personal_Account_Name"];
    $Customer_Type_ID = intval($_COOKIE["Customer_Type_ID"]);
}

?>