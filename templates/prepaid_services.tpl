{include file='header.tpl'}


<div class="container">
    <h2>{$Title}</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Услуга</th>
            <th>Количество</th>
        </tr>
        </thead>
        <tbody>

        {foreach $LoadList as $value}
        <tr>
            <td>{$value["Service_Name"]}</td>
            <td>{$value["Service_Count"]}</td>

        </tr>
        {/foreach}
        </tbody>
    </table>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-0 col-xs-offset-0 ">

            <p></p>

            <p><a href="cabinet.php" class="btn btn-success center-block">Назад в кабинет</a></p>

            <p><a href="index.php" class="btn btn-success center-block">Назад к списку взлетов</a></p>
        </div>
    </div>

</div>


{include file='footer.tpl'}