<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html class='no-js' lang="ru">
<head>
    <TITLE>{$Title}</TITLE>
    <meta name="description" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv='refresh' content='300'>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' href='css/normalize.css'>
    <link rel="stylesheet" href="remodal/remodal.css">
    <link rel="stylesheet" href="remodal/remodal-default-theme.css">
    <link rel='stylesheet' href='css/main.css'>

</head>
<body>