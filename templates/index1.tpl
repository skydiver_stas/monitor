<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html class='no-js' lang="ru">
<head>
	<TITLE>{$Title}</TITLE>
	<meta name="description" content="">
	<meta charset='windows-1251'>
	<meta http-equiv='x-ua-compatible' content='ie=edge'>
	<meta http-equiv='refresh' content='300'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<link rel='stylesheet' href='css/normalize.css'>
	<link rel="stylesheet" href="remodal/remodal.css">
	<link rel="stylesheet" href="remodal/remodal-default-theme.css">
	<link rel='stylesheet' href='css/main.css'>
	<script src='js/modernizr.js'></script>
	<script src='js/jquery-1.12.2.min.js'></script>
	<script src='remodal/remodal.js'></script>
</head>
<body>
<div class="monitor-page-title">
	{$Title}
</div>

<table>
	<tr width="100%">
	{for $columns=0 to $LoadListCnt-1 step 1}
		<td width="{100/$LoadListCnt}%"></td>
	<!-- � ������ ������� ������� ������� ������ ������ -->
	{/for}
	</tr>
</table>

<!-- ������ ������ -->
{$Maxload=16}
<table class="checkintable"><tbody>
	<!-- ���������� � ������� ������ -->
	<tr><td>
		<div class='monitor-page-infobar-item'>����� � {$Loads[$columns]['Load_Number']}&nbsp;</div>
		<div class='monitor-page-infobar-item'>�������� {if $Loads[$columns]['LoadQty'] gt 0}{$Loads[$columns]['LoadQty']}{else}���{/if}&nbsp;{$Loads[$columns]['Seats']}</div>
		<div class='monitor-page-infobar-item'>�������� {$Loads[$columns]['MINUTES']}&nbsp;{$Loads[$columns]['Minutes']}</div>
	    </td>
	</tr>
{foreach $LoadList[$columns] as $customer}
	<tr class="checkintable-row {if $customer.Rank is not div by 2}zebra-dark{/if}">
		<td class="checkintable-spacecell"></td>
		<td class="checkintable-cell checkintable-rownum">{$customer.RowNum}</td>
		<td class="checkintable-cell checkintable-name">{$customer.Customer_Name}&nbsp;</td>
		<td class="checkintable-cell checkintable-exercise">{$customer.Exercise_Name}</td>
		<td class="checkintable-spacecell"></td>
	</tr>
	{$Maxload=$Maxload-1}
{foreachelse}
	<tr class="checkintable-row"><td class="checkintable-cell" colspan="3" align="center">�� ������ ���� ������!</td></tr>
	{$Maxload=15}
{/foreach}
{for $customer=1 to $Maxload}
	<tr class="checkintable-row">
		<td class="checkintable-spacecell"></td>
		<td class="checkintable-cell checkintable-rownum">&nbsp;</td>
		<td class="checkintable-cell" colspan="1">&nbsp;</td>
		<td class="checkintable-cell checkintable-exercise">&nbsp;</td>
		<td class="checkintable-spacecell"></td>
	</tr>
{/for}
</tbody></table>

{if isset($msg)}
	{include 'error.tpl'}
<script src='js/main.js'></script>
{/if}
</body>
</html>