
<!-- форма регистрации -->
<div class='remodal login-form-dialog' data-remodal-id="login-form">
	<div data-remodal-action="close" class="remodal-close"></div>
	<div class='dialog-title'>Вход</div>
	<form class='login-form g-form' action='{$smarty.server.PHP_SELF}' method='post'>
		<div class='g-form-field-holder'>
			<label for='login_username'>Номер лицевого счета</label>
			<input class='t-input' name='login[username]' id='login_username' />
		</div>
		<div class='g-form-field-holder'>
			<label for='login_password'>PIN-код</label>
			<input class='t-input' name='login[password]' id='login_password' />
		</div>
		<div class='login-form-submit-holder'>
			<button class='login-form-submit-button' type='submit'>Войти</button>
		</div>
	</form>
</div>
