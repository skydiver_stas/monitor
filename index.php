<?php

require 'connect_to_db.php';
require 'libs/Smarty.class.php';



// work hours are from 8 to 20
$d = getdate(); // ������������ ������� �����

if ($d['hours'] < 8 || $d['hours'] > 21) {

	$smarty = new Smarty;
	$smarty->debugging = false;
	$smarty->caching = false;
	$smarty->cache_lifetime = 300;

	$smarty->assign("Title", 'DZ Mayskoe');
	$smarty->display('working_time.tpl');
	die;
}



/*Get Loads List*/
$params = array(array(1, SQLSRV_PARAM_IN));
$tsql = "{ CALL dbo.LOAD_GetList ( ? ) }";
$stmt = sqlsrv_query( $conn, $tsql, $params);
if( $stmt === false )
{
     echo "Error in executing query.</br>";
     die( print_r( sqlsrv_errors(), true));
}
$Loads = array();
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
	$row['Seats'] = num2word($row['LoadQty'],array("место","места","мест"));
	$Loads[] = $row;
}


$LoadNum = 0; $flag = false;
$Load_ID = isset($Load_ID) ? intval($Load_ID) : 0;
$flag = array_search($Load_ID,arrayColumn($Loads,'Load_ID')) === false ? 0 : 1;
if (!$flag) $Load_ID = $Loads[0][Load_ID];	 // Load_ID does not exist
else $LoadNum = array_search($Load_ID,arrayColumn($Loads,'Load_ID'));
$CurrentLoad = $Loads[$LoadNum];




/* Do it action if loads not reserved */
if (isset($reg))
if ($CurrentLoad[Rezerved] == 0) {
switch ($reg) {
case "reg":
	$tsql = "{ CALL dbo.Load_List_Add (?,?,?,?,?,?) }";
	$params = array(
		array($Load_ID, SQLSRV_PARAM_IN),		//Load_ID
		array(NULL, SQLSRV_PARAM_IN),			//Exercise_ID
		array($Personal_Account_ID, SQLSRV_PARAM_IN),	//Personal_Account_ID
		array(NULL, SQLSRV_PARAM_IN),			//Parachute_System_ID
		array(NULL, SQLSRV_PARAM_IN),			//Remark
		array($Jump_Type_ID, SQLSRV_PARAM_IN));		//Jump_Type_ID
	break;
case "unreg":
	$tsql = "{ CALL dbo.LOAD_LIST_Del (?,?) }";
	$params = array(
		array($Load_List_ID, SQLSRV_PARAM_IN),		//Load_List_ID
		array($Personal_Account_ID, SQLSRV_PARAM_IN)	//Personal_Account_ID
	);
	break;
/*
case "own":	echo "Rezervation";
	if ($Personal_Account_ID == $Owner_ID) {
		$tsql = "{ CALL dbo.LOAD_LIST_Edit (?,?,?,?,?) }";
		$params = array(
			array($Load_ID, SQLSRV_PARAM_IN),		//Load_ID
			array($Load_List_ID, SQLSRV_PARAM_IN),		//Load_List_ID
			array($Personal_Account_ID, SQLSRV_PARAM_IN),	//Personal_Account_ID
			array($Exercise_ID, SQLSRV_PARAM_IN),		//Exercise_ID
			array(NULL, SQLSRV_PARAM_IN)			//Parachute_System_ID
		);
		// Customer_ID with Exercise_ID access
	}
	break;
*/
default:
	$params = array();
	$tsql = "";
	break;
}; //switch

$stmt = sqlsrv_query( $conn, $tsql, $params);
if( $stmt === false ) {
	if (($errors = sqlsrv_errors()) != null )
		foreach ($errors as $error)
			$msg[] = iconv("CP866", "UTF-8",
				str_replace('[Microsoft][SQL Server Native Client 10.0][SQL Server]','',$error['message']));
}
} //check rezerved
else {
	$msg[] = "Load rezerved!";
}


$LoadList = array();
if ($Load_ID > 0) {
/*List of Current Load */
$params = array(array($Load_ID, SQLSRV_PARAM_IN));
$tsql = "{ CALL dbo.LOAD_LIST_GetList( ? ) }";
$stmt = sqlsrv_query( $conn, $tsql, $params);
if( $stmt === false )
{
     echo "Error in executing query.</br>";
     die( print_r( sqlsrv_errors(), true));
}
print_r($LoadList);

$LoadList = array();
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
	if ($row['Rank'] != null)
		$LoadList[] = $row;
}
}


$InLoad = (array_search($Personal_Account_ID, arrayColumn($LoadList,'Personal_Account_ID')) === false ? 0 : 1);

// comment this block after store exercise template!!!
// logged and does not load member
if ($Personal_Account_ID != 0 && $InLoad != 1) {
	$tsql = "SELECT convert(int, Jump_Type_ID) as Jump_Type_ID, convert(varchar(100),Jump_Type_Name) as Jump_Type_Name FROM Jump_Type WHERE Exercise_ID IS NOT NULL";
	$stmt = sqlsrv_query( $conn, $tsql, $params);
	if( $stmt === false ) {
		echo "Error in executing query.</br>";
		die( print_r( sqlsrv_errors(), true));
	}
	$Jump_Type = array();
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
		$Jump_Type[] = $row;


}

/* Free statement and connection resources. */
sqlsrv_free_stmt( $stmt);
sqlsrv_close( $conn);



$smarty = new Smarty;
//$smarty->force_compile = true;
$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 300;

$smarty->assign("Title", 'DZ Mayskoe');
$smarty->assign("CurrentLoad", $CurrentLoad);
$smarty->assign("LoadList", $LoadList);
$smarty->assign("Loads", $Loads);

$smarty->assign("Personal_Account_Name", $Personal_Account_Name);
$smarty->assign("Personal_Account_ID", $Personal_Account_ID);
$smarty->assign("Customer_Type_ID", $Customer_Type_ID);

//Check includes user in list of current load
$smarty->assign("InLoad", $InLoad);
$smarty->assign("Minutes", num2word($CurrentLoad['MINUTES'],array("минута","минуты","минут")));
$smarty->assign("Jump_Type", $Jump_Type);
$smarty->assign("msg", $msg);


$smarty->display('index.tpl');

//-------------------------------------------------/
// 
function num2word($n, $words) {
	$cases = array(2,0,1,1,1,2);
	return $words[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}

function arrayColumn(array $array, $columnkey, $indexkey = null ) {
	$result = array();
	foreach ($array as $subArray) {
		if (!is_array($subArray)) {
			continue;
		} elseif (is_null($indexkey) && array_key_exists($columnkey, $subArray))  {
			$result[] = $subArray[$columnkey];
		} elseif (array_key_exists($indexket, $subArray)) {
			if (is_null($columnkey)) {
				$result[$subAray[$indexkey]] = $subArray;
			} elseif (array_key_exists($columnkey, $subArray)) {
				$result[$subArray[$indexkey]] = $subArray[$columnkey];
			}
		}
	}
	return $result;
}

?>

