<?php

require 'connect_to_db.php';

if(!$Personal_Account_ID) {
  header('Location:/index.php');
  exit;
}
else {
    /* Personal account report */
//    $Date_End = new DateTime('tomorrow');
    $Date_Beg = new DateTime('2019-05-12');//date_sub($Date_End, new DateInterval('P1M'));
  $Date_End = new DateTime('2019-07-01');

    $tsql = "{ CALL dbo.PERSONAL_ACCOUNT_Rep (?,?,?,?,?,?) }";
    $options = array('ReturnDatesAsStrings' => true);
    $params = array(
        array(&$Date_Beg, SQLSRV_PARAM_IN, null, SQLSRV_SQLTYPE_DATETIME),
        array(&$Date_End, SQLSRV_PARAM_IN, null, SQLSRV_SQLTYPE_DATETIME),
        array(&$Personal_Account_ID, SQLSRV_PARAM_IN, null, SQLSRV_SQLTYPE_INT),
        array(NULL, SQLSRV_PARAM_IN),
        array(NULL, SQLSRV_PARAM_IN),
        array(NULL, SQLSRV_PARAM_IN));

    $stmt = sqlsrv_query( $conn, $tsql, $params, $options);
    $Personal_Account_Report = array();

//    if (sqlsrv_execute($stmt) === true) {
      while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
        $Personal_Account_Report[] = $row;
      }

      var_dump($Personal_Account_Report);
//    }



    require 'libs/Smarty.class.php';
    $smarty = new Smarty;

    $smarty->debugging = false;
    $smarty->caching = false;
    $smarty->cache_lifetime = 300;

    $smarty->assign("Title", 'DZ Mayskoe');

    $smarty->assign("Personal_Account_Name", $Personal_Account_Name);
    $smarty->assign("Personal_Account_ID", $Personal_Account_ID);
    $smarty->assign("Customer_Type_ID", $Customer_Type_ID);

    $smarty->display('movements_funds.tpl');
}


?>
