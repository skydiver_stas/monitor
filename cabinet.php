<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 06.08.17
 * Time: 09:55
 */

require 'connect_to_db.php';

if($Personal_Account_ID != null) {
    require 'libs/Smarty.class.php';
    $smarty = new Smarty;

    $smarty->debugging = false;
    $smarty->caching = false;
    $smarty->cache_lifetime = 300;

    $smarty->assign("Title", 'Кабинет');
    $smarty->display('cabinet.tpl');

} else {
    header('Location:/index.php');
    exit;
}
?>
