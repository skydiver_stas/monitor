USE [manifesto]
GO
/****** Object:  StoredProcedure [dbo].[PERSONAL_ACCOUNT_Rep]    Script Date: 04/30/2017 12:00:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Имя модуля	PERSONAL_ACCOUNT_Rep
Автор		Голикофф Сх2
Дата создания	19.05.2006
Действие	Отчет о движении средств на лицевом счету


Параметры	@Date_Beg		datetime,  -- Дата с
		@Date_End		datetime,  -- Дата по
		@Personal_Account_ID	int = NULL,  -- Идентификатор выпускающего
		@Is_Debet		bit = NULL, -- Показать дебет/кредит
		@Doc_Type_ID_List	varchar(10)	-- Идентификатор типа документа

Результат	нет

Возврат		0 - Нет ошибок
		-1 - Оператор не найден
		-2 - Оператор заблокирован
		-3 - Привилегия не найдена
		-4 - Операция не найдена
		-5 - Нет прав
		-6 - Ошибка

		-7 - Ошибка формирования отчета
*/

ALTER                     PROCEDURE [dbo].[PERSONAL_ACCOUNT_Rep]
--CREATE PROCEDURE dbo.PERSONAL_ACCOUNT_Rep
	@Date_Beg		datetime,  -- Дата с
	@Date_End		datetime,  -- Дата по
	@Personal_Account_ID	int = NULL,  -- Идентификатор выпускающего
	@Is_Debet		bit = NULL, -- Показать дебет/кредит
	@Doc_Type_ID_List	varchar(4000),	-- Идентификатор типов документов
	@Operation_Type		varchar(25)	-- Тип операции

AS
BEGIN

SET NOCOUNT ON
SET IMPLICIT_TRANSACTIONS OFF

DECLARE	@iResult int, @cResult varchar(218)


CREATE TABLE #PA_Rep (
	Doc_Version_ID 		uniqueidentifier,
	Doc_Number		varchar(25) collate Ukrainian_CI_AI,
	Doc_Type_ID		varchar(10) collate Ukrainian_CI_AI,
	Doc_Type_Name 		varchar(254) collate Ukrainian_CI_AI,
	Operation_Date		datetime,
	Personal_Account_ID	int,
	Personal_Account_Number varchar(254) collate Ukrainian_CI_AI,
	Customer_ID		int,
	Customer_Name		varchar(254) collate Ukrainian_CI_AI,
	Operation_Type		varchar(25) collate Ukrainian_CI_AI,
	Service_Name		varchar(254) collate Ukrainian_CI_AI,
	Summa_Out		money,
	Summa_In		money,
	Summa_Rest_Begin	money,
	Summa_Rest_End		money,
	Comment			varchar(4000) collate Ukrainian_CI_AI,
	SortOrder		int
)


CREATE TABLE #PA_Rep2 (
	Doc_Version_ID 		uniqueidentifier,
	Doc_Number		varchar(25) collate Ukrainian_CI_AI,
	Doc_Type_ID		varchar(10) collate Ukrainian_CI_AI,
	Doc_Type_Name 		varchar(254) collate Ukrainian_CI_AI,
	Operation_Date		datetime,
	Personal_Account_ID	int,
	Personal_Account_Number varchar(254) collate Ukrainian_CI_AI,
	Customer_ID		int,
	Customer_Name		varchar(254) collate Ukrainian_CI_AI,
	Operation_Type		varchar(25) collate Ukrainian_CI_AI,
	Service_Name		varchar(254) collate Ukrainian_CI_AI,
	Summa_Out		money,
	Summa_In		money,
	Summa_Rest_Begin	money,
	Summa_Rest_End		money,
	Comment			varchar(4000) collate Ukrainian_CI_AI,
	SortOrder		int,
	Key_ID			int IDENTITY(1,1),
	Summa_INOUT		money,
	Summa_RestEnd		money
)


-- Проверить привилегии оператора
SELECT	@iResult = dbo._ACCESS_STATUS_Get('personal_account_rep', 's')
IF	@iResult <> 0
BEGIN
	SELECT	@cResult = 
		CASE @iResult
		WHEN -1 THEN 'Оператор не найден'
		WHEN -2 THEN 'Оператор заблокирован'
		WHEN -3 THEN 'Привилегия не найдена'
		WHEN -4 THEN 'Операция не найдена'
		WHEN -5 THEN 'Нет прав на получение отчета'
		ELSE	'Ошибка проверки прав'
		END
	GOTO	ERROR
END



-- Переменные для записи в лог
DECLARE	@Date_Beg_New varchar(254),
	@Date_End_New varchar(254),
	@Personal_Account_ID_New varchar(254),
	@Is_Debet_New varchar(254),
	@Operation_Type_New varchar(254)

-- Получение значений для записи в лог
SELECT	@Date_Beg_New = CONVERT(varchar(254), @Date_Beg),
	@Date_End_New = CONVERT(varchar(254), @Date_End),
	@Personal_Account_ID_New = CONVERT(varchar(254), @Personal_Account_ID),
	@Is_Debet_New = CONVERT(varchar(254), @Is_Debet),
	@Operation_Type_New = CONVERT(varchar(254), @Operation_Type)





/*
1. ФО движение средств на лицевом счету по клиенту за период. включает 
документы зачисления, списания, перемещения, переоценки, а также суммы 
расходов по каждому прыжку. 

строка состоит: дата операции, №документа, 
тип документа, дебет, кредит, коментарий (указывается "№взлета-ЛА" для 
документов взлета).
*/



-- IN  Зачисления на счет					1
-- IN  Перемещение  средств на					5
-- IN  Покупка услуг у кого-то					10
-- IN  Списание услуг 						15
-- IN  Взлеты парашютистов (вип скидка)				20	
-- IN  Взлеты парашютистов (предоставление дополнительных услуг)25
-- IN  Взлеты инструкторов					30
-- IN  Аренды ПС (сдающие в аренду)				35
-- IN  VIP-скидки пассажиров					37

-- IN  Полет(предоставление услуги)				38
-- IN  Полет(предоставление дополнительной услуги)		39

-- IN/OUT Покупка услуг						40


-- OUT Списания со счета					45
-- OUT Перемещение  средств c					50
-- OUT Взлеты парашютистов (оплата прыжка)			55
-- OUT Взлеты парашютистов (дополнительные услуги)		60
-- OUT Взлеты парашютистов (доплата)				65
-- OUT Аренды ПС (арендаторы)					70
-- OUT Предоплата услуг						75
-- OUT Взлеты пассажиров					80
-- OUT Доплаты пассажиров					85

-- OUT Оплата услуг полета(оплата полета)			90
-- OUT Оплата услуг полета(дополнительные услуги)		95
-- OUT Оплата услуг полета(аренда ЛА)				100



-- Взлеты парашютистов(оплата прыжка)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ju.Personal_Account_ID,
	pa.Personal_Account_Number,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		ju.Customer_ID
	     ELSE	
		ju.Team_ID
	END AS Customer_ID,

	CASE WHEN (ju.Team_ID IS NULL) THEN
		cu.Customer_Name
	     ELSE	
		te.Customer_Name
	END AS Customer_Name,
	'оплата прыжка',
	se.Service_Name,
--	ju.Service_Price * (1 - IsNULL(ju.VIP_Discount,0)/100) AS Summa_Out,
	ju.Service_Price AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	55 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = ju.Customer_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ju.Personal_Account_ID)
	
	JOIN Service se
	ON (se.Service_ID = ju.Service_ID)

	LEFT JOIN Customer te
	ON (te.Customer_ID = ju.Team_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ju.Personal_Account_ID = @Personal_Account_ID))
	
	AND (ju.Is_Purchased_Service = 0)

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END







-- Взлеты парашютистов(дополнительные услуги)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ju.Personal_Account_ID,
	pa.Personal_Account_Number,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		ju.Customer_ID
	     ELSE	
		ju.Team_ID
	END AS Customer_ID,

	CASE WHEN (ju.Team_ID IS NULL) THEN
		cu.Customer_Name
	     ELSE	
		te.Customer_Name
	END AS Customer_Name,
	'дополнительные услуги',
	NULL,
	SUM(jr.Rate_Price * jr.Rate_Count) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	60 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = ju.Customer_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ju.Personal_Account_ID)

	LEFT JOIN Customer te
	ON (te.Customer_ID = ju.Team_ID)

	JOIN Jump_Rate jr
	ON (jr.Jump_ID = ju.Jump_ID)
	AND (jr.Is_Additional = 1)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ju.Personal_Account_ID = @Personal_Account_ID))
	

GROUP BY
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ju.Personal_Account_ID,
	pa.Personal_Account_Number,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		ju.Customer_ID
	     ELSE	
		ju.Team_ID
	END,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		cu.Customer_Name
	     ELSE	
		te.Customer_Name
	END,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END






-- Взлеты парашютистов(предоставление дополнительных услуг)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'предоставление доп услуг',
	NULL,
	NULL AS Summa_Out,
	SUM(IsNULL(jr.Personal_Payment,0) * jr.Rate_Count) AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	25 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)

	JOIN Jump_Rate jr
	ON (jr.Jump_ID = ju.Jump_ID)
	AND (jr.Personal_Account_ID IS NOT NULL)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = jr.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)



WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (jr.Personal_Account_ID = @Personal_Account_ID))
	

GROUP BY
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END
















-- Взлеты парашютистов (доплата)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ju.Personal_Account_ID,
	pa.Personal_Account_Number,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		ju.Customer_ID
	     ELSE	
		ju.Team_ID
	END AS Customer_ID,

	CASE WHEN (ju.Team_ID IS NULL) THEN
		cu.Customer_Name
	     ELSE	
		te.Customer_Name
	END AS Customer_Name,
	'доплата',
	NULL,
	ju.Additional_Payment AS Summa_Out,

	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	65 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = ju.Customer_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ju.Personal_Account_ID)

	LEFT JOIN Customer te
	ON (te.Customer_ID = ju.Team_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ju.Personal_Account_ID = @Personal_Account_ID))

	AND (ju.Additional_Payment IS NOT NULL)
	AND (ju.Additional_Payment <> 0)

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END






-- Взлеты парашютистов (вип скидка)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ju.Personal_Account_ID,
	pa.Personal_Account_Number,
	CASE WHEN (ju.Team_ID IS NULL) THEN
		ju.Customer_ID
	     ELSE	
		ju.Team_ID
	END AS Customer_ID,

	CASE WHEN (ju.Team_ID IS NULL) THEN
		cu.Customer_Name
	     ELSE	
		te.Customer_Name
	END AS Customer_Name,
	'VIP - скидка',
	NULL,
	NULL AS Summa_Out,
	(ju.Service_Price * IsNULL(ju.VIP_Discount, 0)/100) AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	20 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = ju.Customer_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ju.Personal_Account_ID)

	LEFT JOIN Customer te
	ON (te.Customer_ID = ju.Team_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ju.Personal_Account_ID = @Personal_Account_ID))
	
	AND (ju.VIP_Discount IS NOT NULL)
	AND (ju.VIP_Discount <> 0)

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END






















-- Взлеты инструкторов
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	jp.Personal_Account_ID,
	pa.Personal_Account_Number,
	jp.Customer_ID,
	cu.Customer_Name,	
	'оплата персоналу',
	se.Service_Name,
	NULL AS Summa_Out,
	SUM(jpr.Rate_Price * jpr.Rate_Count) AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	30 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Jump_Personal jp
	ON (jp.Jump_ID = ju.Jump_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = jp.Customer_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = jp.Personal_Account_ID)
	JOIN Jump_Personal_Rate jpr
	ON (jpr.Jump_ID = jp.Jump_ID)
	AND (jpr.Instructor_Type_ID = jp.Instructor_Type_ID)

	JOIN Service se
	ON (se.Service_ID = ju.Service_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')

	AND ((@Personal_Account_ID IS NULL)
		OR (jp.Personal_Account_ID = @Personal_Account_ID))

GROUP BY
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	jp.Personal_Account_ID,
	pa.Personal_Account_Number,
	jp.Customer_ID,
	cu.Customer_Name,	
	se.Service_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END






-- Аренды ПС (арендаторы)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'аренда ПС',
	NULL,
	psr.Rent_Summa_From AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	70 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)

	JOIN Parachute_System_Rent psr
	ON (psr.Jump_ID = ju.Jump_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = psr.Personal_Account_ID_From)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (psr.Personal_Account_ID_From = @Personal_Account_ID))
	

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


-- Аренды ПС (сдающие в аренду)
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'аренда ПС',
	NULL,
	NULL AS Summa_Out,
	psr.Rent_Summa_To AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	35 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Jump ju
	ON (ju.Doc_Version_ID = fl.Doc_Version_ID)

	JOIN Parachute_System_Rent psr
	ON (psr.Jump_ID = ju.Jump_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = psr.Personal_Account_ID_To)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (psr.Personal_Account_ID_To = @Personal_Account_ID))
	

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END





--Зачисления на счет
INSERT INTO #PA_Rep
SELECT 	de.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	de.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'зачисление на счет',
	NULL,
	NULL AS Summa_Out,
	de.Deposit_Summ AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	1 AS SortOrder

FROM	Deposit de
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = de.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = de.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (de.Personal_Account_ID = @Personal_Account_ID))
	

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END




--Списания со счета
INSERT INTO #PA_Rep
SELECT 	ch.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ch.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'списание со счета',
	NULL,
	ch.Charge_Off_Summa  AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	45 AS SortOrder

FROM	Charge_Off ch
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = ch.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ch.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ch.Personal_Account_ID = @Personal_Account_ID))

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


--Перемещение  средств c
INSERT INTO #PA_Rep
SELECT 	tr.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	tr.Personal_Account_ID_From,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'перемещение средств',
	NULL,
	tr.Summa AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	50 AS SortOrder

FROM	Transfer tr
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = tr.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = tr.Personal_Account_ID_From)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (tr.Personal_Account_ID_From = @Personal_Account_ID))

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


--Перемещение  средств на
INSERT INTO #PA_Rep
SELECT 	tr.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	tr.Personal_Account_ID_To,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'перемещение средств',
	NULL,
	NULL AS Summa_Out,
	tr.Summa AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	5 AS SortOrder

FROM	Transfer tr
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = tr.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = tr.Personal_Account_ID_To)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (tr.Personal_Account_ID_To = @Personal_Account_ID))

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


--Предоплата услуг
INSERT INTO #PA_Rep
SELECT 	ps.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'предоплата услуг',
	se.Service_Name,
	SUM(psd.Service_Summ) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	75 AS SortOrder

FROM	Purchased_Service ps
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = ps.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ps.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

	JOIN Purchased_Service_Detail psd
	ON (psd.Doc_Version_ID = ps.Doc_Version_ID)

	JOIN Service se
	ON (se.Service_ID = psd.Service_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ps.Personal_Account_ID = @Personal_Account_ID))

GROUP BY
	ps.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	se.Service_Name,
	cu.Customer_Name

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END



--Покупка услуг
INSERT INTO #PA_Rep 
SELECT 	ss.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ss.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'покупка услуг',
	se.Service_Name,
--	NULL,
	CASE WHEN SUM(ssd.Service_Summ) < 0 THEN NULL
		ELSE SUM(ssd.Service_Summ)
	END AS Summa_Out,

	CASE WHEN SUM(ssd.Service_Summ) < 0 THEN SUM(ssd.Service_Summ)*-1
		ELSE NULL
	END AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	40 AS SortOrder

FROM	Service_Sale ss
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = ss.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ss.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

	JOIN Service_Sale_Detail ssd
	ON (ssd.Doc_Version_ID = ss.Doc_Version_ID)

	JOIN Service se
	ON (se.Service_ID = ssd.Service_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ss.Personal_Account_ID = @Personal_Account_ID))

GROUP BY
	ss.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ss.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	se.Service_Name,
	cu.Customer_Name	


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END





-- Покупка услуг у кого-то 
INSERT INTO #PA_Rep 
SELECT 	ss.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ss.Personal_Account_ID_From,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'продажа услуг',
	se.Service_Name,
	NULL AS Summa_Out,

	SUM(ssr.Personal_Payment * ssr.Rate_Count * ssd.Service_Count)  AS Summa_In,

	NULL,
	NULL,
	'' AS Comment,
	10 AS SortOrder

FROM	Service_Sale ss
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = ss.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ss.Personal_Account_ID_From)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

	JOIN Service_Sale_Detail ssd
	ON (ssd.Doc_Version_ID = ss.Doc_Version_ID)

	JOIN Service_Sale_Rate ssr
	ON (ssr.Doc_Version_ID = ssd.Doc_Version_ID)
	AND (ssr.Service_ID = ssd.Service_ID)

	JOIN Service se
	ON (se.Service_ID = ssd.Service_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (ss.Personal_Account_ID_From IS NOT NULL)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ss.Personal_Account_ID_From = @Personal_Account_ID))

GROUP BY
	ss.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ss.Personal_Account_ID_From,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	se.Service_Name,
	cu.Customer_Name	


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


















--Списание услуг 
INSERT INTO #PA_Rep
SELECT 	ps.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'списание услуг',
	se.Service_Name,
	NULL AS Summa_Out,
	SUM(2*psd.Service_Count * Service_Price - psd.Service_Count * psd.Service_Price_Full) AS Summa_In,
	NULL,
	NULL,
	'' AS Comment,
	15 AS SortOrder

FROM	Purchased_Service_Overvalue ps
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = ps.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ps.Personal_Account_ID)

	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)

	JOIN Purchased_Service_Overvalue_Detail psd
	ON (psd.Doc_Version_ID = ps.Doc_Version_ID)

	JOIN Service se
	ON (se.Service_ID = psd.Service_ID)

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ps.Personal_Account_ID = @Personal_Account_ID))

GROUP BY
	ps.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	se.Service_Name,
	cu.Customer_Name

IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END



-- Взлеты пассажиров
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'оплата полета(пассажир)',
	se.Service_Name,
	SUM(pr.Rate_Price * pr.Rate_Count) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	80 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Passenger ps
	ON (ps.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ps.Personal_Account_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	
	JOIN Service se
	ON (se.Service_ID = ps.Service_ID)

	JOIN Passenger_Rate pr
	ON (pr.Doc_Version_ID = ps.Doc_Version_ID)
	AND (pr.Personal_Account_ID = ps.Personal_Account_ID)	

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ps.Personal_Account_ID = @Personal_Account_ID))
GROUP BY
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	se.Service_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


-- Доплаты пассажиров
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	ps.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'доплата(пассажир)',
	se.Service_Name,
	ps.Additional_Payment AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	85 AS SortOrder

FROM	Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt
	ON (dt.Doc_Type_ID = do.Doc_Type_ID)
	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN Passenger ps
	ON (ps.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = ps.Personal_Account_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	
	JOIN Service se
	ON (se.Service_ID = ps.Service_ID)


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (ps.Personal_Account_ID = @Personal_Account_ID))


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END




-- VIP-скидка пассажиров
INSERT INTO #PA_Rep

SELECT	Doc_Version_ID,
	Doc_Number,
	Doc_Type_ID,
	Doc_Type_Name,
	Operation_Date,
	Personal_Account_ID,
	Personal_Account_Number,
	Customer_ID,
	Customer_Name,
	Operation_Type,
	Service_Name,
	Summa_Out,
	(Summa_In  + Additional_Payment) * 0.01 * VIP_Discount,
	Summa_Rest_Begin,
	Summa_Rest_End,
	Comment,
	SortOrder

FROM
(
	SELECT 	fl.Doc_Version_ID,
		do.Doc_Number,
		dt.Doc_Type_ID,
		dt.Doc_Type_Name,
		dv.Operation_Date,
		ps.Personal_Account_ID,
		pa.Personal_Account_Number,
		cu.Customer_ID,
		cu.Customer_Name,
		'VIP-скидка(пассажир)' AS Operation_Type,
		se.Service_Name,
		NULL AS Summa_Out,
	--	(SUM(pr.Rate_Price * pr.Rate_Count) + ps.Additional_Payment) * 0.01 * ps.VIP_Discount AS Summa_In,
		SUM(pr.Rate_Price * pr.Rate_Count) AS Summa_In,
		NULL AS Summa_Rest_Begin,
		NULL AS Summa_Rest_End,
		(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
		37 AS SortOrder,
		ps.Additional_Payment,
		ps.VIP_Discount
	
	FROM	Flight fl
		JOIN Doc_Version dv
		ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
		JOIN Doc do
		ON (do.Doc_Version_ID = dv.Doc_Version_ID)
		JOIN Doc_Type dt	
		ON (dt.Doc_Type_ID = do.Doc_Type_ID)
		JOIN Aircraft ai
		ON (ai.Aircraft_ID = fl.Aircraft_ID)
		JOIN Aircraft_Type ait
		ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)
	
		JOIN Passenger ps
		ON (ps.Doc_Version_ID = fl.Doc_Version_ID)
		JOIN Personal_Account pa
		ON (pa.Personal_Account_ID = ps.Personal_Account_ID)
		JOIN Customer cu
		ON (cu.Customer_ID = pa.Customer_ID)
		
		JOIN Service se
		ON (se.Service_ID = ps.Service_ID)
	
		JOIN Passenger_Rate pr
		ON (pr.Doc_Version_ID = ps.Doc_Version_ID)
		AND (pr.Personal_Account_ID = ps.Personal_Account_ID)	
	
	WHERE	(dv.Operation_Date >= @Date_Beg)
		AND (dv.Operation_Date <= @Date_End)
		AND (dv.Doc_Status_ID = 'ок')
		
		AND ((@Personal_Account_ID IS NULL)
			OR (ps.Personal_Account_ID = @Personal_Account_ID))
	GROUP BY
		fl.Doc_Version_ID,
		do.Doc_Number,
		dt.Doc_Type_ID,
		dt.Doc_Type_Name,
		dv.Operation_Date,
		ps.Personal_Account_ID,
		pa.Personal_Account_Number,
		cu.Customer_ID,
		cu.Customer_Name,
		se.Service_Name,
		(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name),
		ps.Additional_Payment,
		ps.VIP_Discount
	
) s



IF	@@ERROR <> 0BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END





-- IN  полет(предоставление услуги)				38
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'предоставление услуг',
	se.Service_Name,
	NULL AS Summa_Out,
	SUM(fr.Personal_Payment * fr.Rate_Count) AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	38 AS SortOrder

FROM	FC_Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN FC_Flight_Rate fr
	ON (fr.Doc_Version_ID = dv.Doc_Version_ID)
	--AND (fr.Is_Aircraft_Rate = 0)
	AND (fr.Is_Additional = 0)
 
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = fr.Personal_Account_ID_To)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	
	JOIN Service se
	ON (se.Service_ID = fl.Service_ID)


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))

GROUP BY 
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,	
	se.Service_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END



-- IN  Полет(предоставление дополнительной услуги)		39
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'предоставление доп услуг',
	NULL,
	NULL AS Summa_Out,
	SUM(fr.Personal_Payment * fr.Rate_Count) AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	39 AS SortOrder

FROM	FC_Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN FC_Flight_Rate fr
	ON (fr.Doc_Version_ID = dv.Doc_Version_ID)
	AND (fr.Is_Additional = 1)
 
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = fr.Personal_Account_ID_To)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	



WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))

GROUP BY 
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


-- OUT Оплата услуг полета(оплата полета)			90
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'оплата полета',
	se.Service_Name,
	SUM(fr.Rate_Price * fr.Rate_Count) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	90 AS SortOrder

FROM	FC_Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN FC_Flight_Rate fr
	ON (fr.Doc_Version_ID = dv.Doc_Version_ID)
	AND (fr.Is_Aircraft_Rate = 1)
	AND (fr.Is_Additional = 0)
 
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = fr.Personal_Account_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	
	JOIN Service se
	ON (se.Service_ID = fl.Service_ID)


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))

GROUP BY 
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,	
	se.Service_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


-- OUT Оплата услуг полета(дополнительные услуги)		95
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'дополнительные услуги',
	NULL,
	SUM(fr.Rate_Price * fr.Rate_Count) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	95 AS SortOrder

FROM	FC_Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN FC_Flight_Rate fr
	ON (fr.Doc_Version_ID = dv.Doc_Version_ID)
	AND (fr.Is_Additional = 1)
 
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = fr.Personal_Account_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	


WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))

GROUP BY 
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END


-- OUT Оплата услуг полета(аренда ЛА)				100
INSERT INTO #PA_Rep
SELECT 	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	'аренда ЛА',
	NULL,
	SUM(fr.Rate_Price * fr.Rate_Count) AS Summa_Out,
	NULL AS Summa_In,
	NULL,
	NULL,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name) AS Comment,
	100 AS SortOrder

FROM	FC_Flight fl
	JOIN Doc_Version dv
	ON (dv.Doc_Version_ID = fl.Doc_Version_ID)
	JOIN Doc do
	ON (do.Doc_Version_ID = dv.Doc_Version_ID)
	JOIN Doc_Type dt	ON (dt.Doc_Type_ID = do.Doc_Type_ID)

	JOIN Aircraft ai
	ON (ai.Aircraft_ID = fl.Aircraft_ID)
	JOIN Aircraft_Type ait
	ON (ait.Aircraft_Type_ID = ai.Aircraft_Type_ID)

	JOIN FC_Flight_Rate fr
	ON (fr.Doc_Version_ID = dv.Doc_Version_ID)
	AND (fr.Is_Aircraft_Rate = 0)
	AND (fr.Is_Additional = 0)
 
	JOIN Personal_Account pa
	ON (pa.Personal_Account_ID = fr.Personal_Account_ID)
	JOIN Customer cu
	ON (cu.Customer_ID = pa.Customer_ID)
	

WHERE	(dv.Operation_Date >= @Date_Beg)
	AND (dv.Operation_Date <= @Date_End)
	AND (dv.Doc_Status_ID = 'ок')
	
	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))

GROUP BY 
	fl.Doc_Version_ID,
	do.Doc_Number,
	dt.Doc_Type_ID,
	dt.Doc_Type_Name,
	dv.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	cu.Customer_ID,
	cu.Customer_Name,
	(ait.Aircraft_Type_Name + ' ' +	ai.Aircraft_Name)


IF	@@ERROR <> 0
BEGIN
	SELECT	@iResult = -7, @cResult = 'Ошибка формирования отчета'
	GOTO	ERROR
END



























-- Добавить остаток на начало периода и на конец
UPDATE	#PA_Rep 
SET	Summa_Rest_Begin = s.Summa_Rest_Begin,
	Summa_Rest_End = s.Summa_Rest_End 
FROM	(
	SELECT	pa.Personal_Account_ID,
		IsNULL(re.Summa_Rest,0) AS Summa_Rest_Begin,
		IsNULL(re1.Summa_Rest,0) AS Summa_Rest_End
	FROM	#PA_Rep pa
		LEFT JOIN Rest re
		ON (re.Personal_Account_ID = pa.Personal_Account_ID)
		AND (re.Rest_Date = (
			SELECT	MAX(Rest_Date) 
			FROM 	Rest 
			WHERE 	(Personal_Account_ID = pa.Personal_Account_ID) 
				AND (Rest_Date < @Date_Beg))
		)	
	
		LEFT JOIN Rest re1
		ON (re1.Personal_Account_ID = pa.Personal_Account_ID)
		AND (re1.Rest_Date = (
			SELECT	MAX(Rest_Date) 
			FROM 	Rest 
			WHERE 	(Personal_Account_ID = pa.Personal_Account_ID) 
				AND (Rest_Date <= @Date_End))
		)	
	)s
WHERE	#PA_Rep.Personal_Account_ID = s.Personal_Account_ID


-- Формируем табличку с последовательно пронумерованым списком
INSERT INTO #PA_Rep2
	(Doc_Version_ID,
	Doc_Number,
	Doc_Type_ID,
	Doc_Type_Name,
	Operation_Date,
	Personal_Account_ID,
	Personal_Account_Number,
	Customer_ID,
	Customer_Name,
	Operation_Type,
	Service_Name,
	Summa_Out,
	Summa_In,
	Summa_Rest_Begin,
	Summa_Rest_End,
	Comment,
	SortOrder)
SELECT 	Doc_Version_ID,
	Doc_Number,
	Doc_Type_ID,
	Doc_Type_Name,
	Operation_Date,
	Personal_Account_ID,
	Personal_Account_Number,
	Customer_ID,
	Customer_Name,
	Operation_Type,
	Service_Name,
	Summa_Out,
	Summa_In,
	Summa_Rest_Begin,
	Summa_Rest_End,
	Comment,
	SortOrder 
FROM 	#PA_Rep
ORDER BY Personal_Account_ID, Operation_Date, SortOrder

-- Формируем нарастающий итог
UPDATE	#PA_Rep2
SET
	Summa_INOUT = s.Summa_INOUT,
	Summa_RestEnd = s.Summa_RestEnd 
FROM
(
	SELECT 	
		pa.Key_ID,
		pa.Personal_Account_ID,
		MAX(IsNULL(pa.Summa_In, 0) - IsNULL(pa.Summa_Out,0)) AS Summa_INOUT,
		SUM(IsNULL(pa2.Summa_In, 0) - IsNULL(pa2.Summa_Out,0)) + MAX(IsNULL(pa.Summa_Rest_Begin, 0) + IsNULL(pa.Summa_In, 0) - IsNULL(pa.Summa_Out,0)) AS Summa_RestEnd
	FROM	#PA_Rep2 pa 
		LEFT JOIN #PA_Rep2 pa2
		ON (pa.Personal_Account_ID = pa2.Personal_Account_ID) 
		AND (pa.Key_ID > pa2.Key_ID)
	GROUP BY 	
		pa.Key_ID,
		pa.Personal_Account_ID
)s	
WHERE #PA_Rep2.Key_ID = s.Key_ID


-- запрос
SELECT 	pa.Doc_Version_ID,
	pa.Doc_Number,
	pa.Doc_Type_ID,
	pa.Doc_Type_Name,
	pa.Operation_Date,
	pa.Personal_Account_ID,
	pa.Personal_Account_Number,
	pa.Customer_ID,
	pa.Customer_Name,
	pa.Operation_Type,
	pa.Service_Name,
	pa.Summa_Out,
	pa.Summa_In,
	pa.Summa_Rest_Begin,
	pa.Summa_Rest_End,
	pa.Comment,
	pa.SortOrder,
	pa.Summa_INOUT,
	pa.Summa_RestEnd
 
FROM 	#PA_Rep2 pa
	LEFT JOIN dbo._LIST_CHAR_Parse(@Doc_Type_ID_List) ls
	ON (ls.ID COLLATE Ukrainian_CI_AI = pa.Doc_Type_ID )

WHERE	((@Is_Debet IS NULL) 
	OR ((@Is_Debet = 1) AND (pa.Summa_In IS NOT NULL) AND (pa.Summa_In > 0))
	OR ((@Is_Debet = 0) AND (pa.Summa_Out IS NOT NULL) AND (pa.Summa_Out > 0)))

	AND ((@Doc_Type_ID_List IS NULL)
			OR (ls.ID IS NOT NULL))

	AND ((@Personal_Account_ID IS NULL)
		OR (pa.Personal_Account_ID = @Personal_Account_ID))
	
	AND ((@Operation_Type IS NULL) 
		OR (pa.Operation_Type = @Operation_Type))

	AND ((pa.Summa_Out IS NOT NULL) OR (pa.Summa_In IS NOT NULL))	

ORDER BY pa.Key_ID



SELECT	@iResult = 0, @cResult = ''

ERROR:

DROP TABLE #PA_Rep

-- Запись в лог
DECLARE	@Log_ID uniqueidentifier, @Log_Descr_ID int
EXECUTE	_LOG_DESCR_MULTIPLE_Add
	@Log_ID			= @Log_ID OUTPUT,
	@Log_Descr_ID		= @Log_Descr_ID OUTPUT,
	@Privilege_Name		= 'personal_account_rep',
	@Operation_ID		= 's',
	@Operation_Status	= @iResult,
	@Result			= @cResult,
	@Duration		= 0,

	@Field_Name1	= 'Дата с',
	@Old_Value1	= NULL,
	@New_Value1	= @Date_Beg_New,

	@Field_Name2	= 'Дата по',
	@Old_Value2	= NULL,
	@New_Value2	= @Date_End_New,

	@Field_Name3	= 'Показать дебет/кредит',
	@Old_Value3	= NULL,
	@New_Value3	= @Is_Debet_New,

	@Field_Name4	= 'Идентификатор счета выпускающего',
	@Old_Value4	= NULL,
	@New_Value4	= @Personal_Account_ID_New,

	@Field_Name6	= 'Тип операции',
	@Old_Value6	= NULL,
	@New_Value6	= @Operation_Type_New






IF	@iResult <> 0
	RAISERROR(@cResult, 16, -1)

RETURN	@iResult

END





















